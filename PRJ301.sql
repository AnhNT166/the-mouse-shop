USE [master]
GO
/****** Object:  Database [ProjectPRJ301]    Script Date: 10/30/2023 5:30:50 PM ******/
CREATE DATABASE [ProjectPRJ301]
GO
ALTER DATABASE [ProjectPRJ301] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ProjectPRJ301].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ProjectPRJ301] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET ARITHABORT OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [ProjectPRJ301] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ProjectPRJ301] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ProjectPRJ301] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET  ENABLE_BROKER 
GO
ALTER DATABASE [ProjectPRJ301] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ProjectPRJ301] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ProjectPRJ301] SET  MULTI_USER 
GO
ALTER DATABASE [ProjectPRJ301] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ProjectPRJ301] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ProjectPRJ301] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ProjectPRJ301] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ProjectPRJ301] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ProjectPRJ301] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [ProjectPRJ301] SET QUERY_STORE = ON
GO
ALTER DATABASE [ProjectPRJ301] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [ProjectPRJ301]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 10/30/2023 5:30:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[uID] [int] IDENTITY(1,1) NOT NULL,
	[user] [varchar](255) NULL,
	[pass] [varchar](255) NULL,
	[isSell] [int] NULL,
	[isAdmin] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[uID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 10/30/2023 5:30:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[cid] [int] NOT NULL,
	[cname] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 10/30/2023 5:30:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[OrderID] [int] NULL,
	[productID] [int] NULL,
	[price] [float] NULL,
	[quantity] [int] NULL,
	[totalBill] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 10/30/2023 5:30:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[orderid] [int] IDENTITY(1,1) NOT NULL,
	[orderdate] [date] NULL,
	[totalbill] [decimal](10, 2) NULL,
	[name] [nvarchar](255) NULL,
	[address] [varchar](255) NULL,
	[phone] [varchar](20) NULL,
 CONSTRAINT [PK__Orders__080E37758A67076A] PRIMARY KEY CLUSTERED 
(
	[orderid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[product]    Script Date: 10/30/2023 5:30:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NULL,
	[image] [nvarchar](max) NULL,
	[price] [money] NULL,
	[title] [nvarchar](max) NULL,
	[description] [nvarchar](max) NULL,
	[cateID] [int] NULL,
	[sell_ID] [int] NULL,
 CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (1, N'tuananh123', N'123456', 1, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (2, N'admin', N'admin', 0, 1)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (3, N'hiiameasyanh', N'tuananh123', 1, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (4, N'tuananh', N'tuananh', 1, 0)
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
INSERT [dbo].[Category] ([cid], [cname]) VALUES (1, N'Asus')
INSERT [dbo].[Category] ([cid], [cname]) VALUES (2, N'Corsair')
INSERT [dbo].[Category] ([cid], [cname]) VALUES (3, N'Logitech')
INSERT [dbo].[Category] ([cid], [cname]) VALUES (4, N'Razer')
GO
INSERT [dbo].[OrderDetail] ([OrderID], [productID], [price], [quantity], [totalBill]) VALUES (8, 1, 100, 1, 100)
INSERT [dbo].[OrderDetail] ([OrderID], [productID], [price], [quantity], [totalBill]) VALUES (8, 2, 120, 1, 120)
INSERT [dbo].[OrderDetail] ([OrderID], [productID], [price], [quantity], [totalBill]) VALUES (9, 1, 100, 4, 400)
INSERT [dbo].[OrderDetail] ([OrderID], [productID], [price], [quantity], [totalBill]) VALUES (10, 1, 101, 1, 101)
INSERT [dbo].[OrderDetail] ([OrderID], [productID], [price], [quantity], [totalBill]) VALUES (11, 1, 101, 4, 404)
INSERT [dbo].[OrderDetail] ([OrderID], [productID], [price], [quantity], [totalBill]) VALUES (11, 2, 120, 1, 120)
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([orderid], [orderdate], [totalbill], [name], [address], [phone]) VALUES (6, CAST(N'2023-08-18' AS Date), CAST(270.00 AS Decimal(10, 2)), N'Nguyễn Tuấn Anh', N'Vinh Phúc', N'0326673227')
INSERT [dbo].[Orders] ([orderid], [orderdate], [totalbill], [name], [address], [phone]) VALUES (7, CAST(N'2023-08-20' AS Date), CAST(420.00 AS Decimal(10, 2)), N'Nguyen Tuan Anh', N'Hà N?i', N'0287301067')
INSERT [dbo].[Orders] ([orderid], [orderdate], [totalbill], [name], [address], [phone]) VALUES (8, CAST(N'2023-08-20' AS Date), CAST(220.00 AS Decimal(10, 2)), N'Nguyễn Tuấn Anh', N'Vinh Phúc', N'0326673227')
INSERT [dbo].[Orders] ([orderid], [orderdate], [totalbill], [name], [address], [phone]) VALUES (9, CAST(N'2023-08-20' AS Date), CAST(400.00 AS Decimal(10, 2)), N'Ha Quang Thang', N'Thái Nguyên', N'0969818222')
INSERT [dbo].[Orders] ([orderid], [orderdate], [totalbill], [name], [address], [phone]) VALUES (10, CAST(N'2023-09-09' AS Date), CAST(101.00 AS Decimal(10, 2)), N'Phùng Minh Quang', N'Ð?nh Trung', N'1234567891')
INSERT [dbo].[Orders] ([orderid], [orderdate], [totalbill], [name], [address], [phone]) VALUES (11, CAST(N'2023-10-12' AS Date), CAST(524.00 AS Decimal(10, 2)), N'Phung Minh Quang', N'Dinh Trung, Vinh Yen', N'1234567891')
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[product] ON 

INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID]) VALUES (1, N'CHUỘT GAMING ASUS TUF M3 GEN II', N'https://hanoicomputercdn.com/media/product/73017_chuot_gaming_co_day_asus_tuf_m3_gen_ii_90mp0320_bmua00_1.jpg', 101.0000, N'ASUS TUF Gaming M3 Gen II', N'Asus đã quá nổi tiếng khi đang là một công ty đa công nghệ xuyên quốc gia hàng đầu hiện nay. Được thành lập từ năm 1989, đến nay “ông lớn” này đã có một chỗ đứng rất vững chắc trên thị trường công nghệ với hàng loạt giải thưởng danh giá. Các dòng sản phẩm nổi bật của Asus có thể kể đến như laptop, chuột, điện thoại, tai nghe,... đều được bày bán rộng rãi trên hầu hết các quốc gia trên thế giới. Dễ dàng nhận thấy, thương hiệu này vô cùng được ưa chuộng bởi mức giá ổn định đi kèm với chất lượng vượt trội.', 1, 3)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID]) VALUES (2, N'CHUỘT KHÔNG DÂY GAMING ASUS ROG HARPE ACE AIM LAB BLACK', N'https://hanoicomputercdn.com/media/product/70572_chuot_khong_day_gaming_asus_rog_harpe_ace_aim_lab0_001.jpg', 120.0000, N'CHUỘT KHÔNG DÂY GAMING ASUS ROG HARPE ACE AIM LAB BLACK
', N'Asus đã quá nổi tiếng khi đang là một công ty đa công nghệ xuyên quốc gia hàng đầu hiện nay. Được thành lập từ năm 1989, đến nay “ông lớn” này đã có một chỗ đứng rất vững chắc trên thị trường công nghệ với hàng loạt giải thưởng danh giá. Các dòng sản phẩm nổi bật của Asus có thể kể đến như laptop, chuột, điện thoại, tai nghe,... đều được bày bán rộng rãi trên hầu hết các quốc gia trên thế giới. Dễ dàng nhận thấy, thương hiệu này vô cùng được ưa chuộng bởi mức giá ổn định đi kèm với chất lượng vượt trội.', 1, 1)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID]) VALUES (3, N'CHUỘT GAMING KHÔNG DÂY ASUS ROG KERIS WIRELESS AIMPOINT', N'https://hanoicomputercdn.com/media/product/72534_chuot_gaming_khong_day_asus_rog_keris_wireless_aimpoint_white_90mp02v0_bmua10_4.jpg', 130.0000, N'CHUỘT GAMING KHÔNG DÂY ASUS ROG KERIS WIRELESS AIMPOINT
', N'Asus đã quá nổi tiếng khi đang là một công ty đa công nghệ xuyên quốc gia hàng đầu hiện nay. Được thành lập từ năm 1989, đến nay “ông lớn” này đã có một chỗ đứng rất vững chắc trên thị trường công nghệ với hàng loạt giải thưởng danh giá. Các dòng sản phẩm nổi bật của Asus có thể kể đến như laptop, chuột, điện thoại, tai nghe,... đều được bày bán rộng rãi trên hầu hết các quốc gia trên thế giới. Dễ dàng nhận thấy, thương hiệu này vô cùng được ưa chuộng bởi mức giá ổn định đi kèm với chất lượng vượt trội.', 1, 4)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID]) VALUES (4, N'CHUỘT GAMING KHÔNG DÂY CORSAIR IRON CLAW RGB', N'https://hanoicomputercdn.com/media/product/71911_chuot_khong_day_corsair_iron_claw_rgb_usb_rgb_den_ch_9317011_ap_0001_2.jpg', 140.0000, N'CHUỘT GAMING KHÔNG DÂY CORSAIR IRON CLAW RGB
', N'Corsair hay Corsair Components, Inc., là một công ty phần cứng và thiết bị ngoại vi máy tính của Mỹ có trụ sở tại Fremont, California. Theo thời gian, Corsair đã ngày càng đa dạng hóa những mặt hàng của mình, lấn sân sang nhóm sản phẩm phụ kiện máy tính.', 2, 4)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID]) VALUES (5, N'CHUỘT CORSAIR KATAR PRO', N'https://hanoicomputercdn.com/media/product/55866_chuot_corsair_katar_pro_paw3327_ch_930c011_ap_0001_2.jpg', 150.0000, N'CHUỘT CORSAIR KATAR PRO
', N'Corsair hay Corsair Components, Inc., là một công ty phần cứng và thiết bị ngoại vi máy tính của Mỹ có trụ sở tại Fremont, California. Theo thời gian, Corsair đã ngày càng đa dạng hóa những mặt hàng của mình, lấn sân sang nhóm sản phẩm phụ kiện máy tính.', 2, 4)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID]) VALUES (6, N'CHUỘT CHƠI GAME KHÔNG DÂY CORSAIR HARPOON RGB WIRELESS', N'https://hanoicomputercdn.com/media/product/46391_mouse_corsair_harpoon_rgb_wireless_ch_9311011_ap_0002_1.jpg', 160.0000, N'CHUỘT CHƠI GAME KHÔNG DÂY CORSAIR HARPOON RGB WIRELESS
', N'Corsair hay Corsair Components, Inc., là một công ty phần cứng và thiết bị ngoại vi máy tính của Mỹ có trụ sở tại Fremont, California. Theo thời gian, Corsair đã ngày càng đa dạng hóa những mặt hàng của mình, lấn sân sang nhóm sản phẩm phụ kiện máy tính.', 2, 4)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID]) VALUES (7, N'CHUỘT GAME KHÔNG DÂY LOGITECH PRO X SUPERLIGHT', N'https://hanoicomputercdn.com/media/product/64441_chuot_game_khong_day_logitech_pro_x_superlight_magenta_usb_rgb_hong_0001_2.jpg', 170.0000, N'CHUỘT GAME KHÔNG DÂY LOGITECH PRO X SUPERLIGHT
', N'Logitech từ lâu đã được biết đến là thương hiệu nổi tiếng chuyên sản xuất các thiết bị phụ kiện dành cho máy tính trong đó nổi bật là chuột máy tính.', 3, 1)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID]) VALUES (8, N'CHUỘT GAME KHÔNG DÂY LOGITECH G304', N'https://hanoicomputercdn.com/media/product/59055_chuot_khong_day_logitech_g304_lightspeed_trang_usb_0000_1.jpg', 150.0000, N'CHUỘT GAME KHÔNG DÂY LOGITECH G304
', N'Logitech từ lâu đã được biết đến là thương hiệu nổi tiếng chuyên sản xuất các thiết bị phụ kiện dành cho máy tính trong đó nổi bật là chuột máy tính.', 3, 1)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID]) VALUES (9, N'CHUỘT GAME KHÔNG DÂY LOGITECH G PRO WIRELESS', N'https://hanoicomputercdn.com/media/product/47527_mouse_logitech_g_pro_wireless_gaming_0001_1.jpg', 180.0000, N'CHUỘT GAME KHÔNG DÂY LOGITECH G PRO WIRELESS
', N'Logitech từ lâu đã được biết đến là thương hiệu nổi tiếng chuyên sản xuất các thiết bị phụ kiện dành cho máy tính trong đó nổi bật là chuột máy tính.
', 3, 1)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID]) VALUES (10, N'CHUỘT GAMING KHÔNG DÂY RAZER DEATHADDER V3', N'https://hanoicomputercdn.com/media/product/70676_chuot_gaming_khong_day_razer_deathadder_v3_pro_faker_edition_rz01_04630400_r3m1_0000_1.jpg', 180.0000, N'CHUỘT GAMING KHÔNG DÂY RAZER DEATHADDER V3
', N'Chuột Razer là thương hiệu đến từ nước Mỹ trực thuộc công ty chuyên sản xuất các sản phẩm công nghệ dành cho game thủ như: Laptop, máy tính bảng, các thiết bị ngoại vi (chuột, bàn phím,...), phụ kiện.
', 4, 3)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID]) VALUES (16, N'Nguyễn Tuấn Anh', N'https://images.pexels.com/photos/268533/pexels-photo-268533.jpeg?cs=srgb&dl=pexels-pixabay-268533.jpg&fm=jpg', 150.0000, N'123123124', N'ABC', 1, 3)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID]) VALUES (17, N'Nguyễn Tuấn Anh', N'https://images.unsplash.com/photo-1575936123452-b67c3203c357?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aW1hZ2V8ZW58MHx8MHx8fDA%3D&w=1000&q=80', 150.0000, N'abcaagasgsa', N'ABC', 1, 3)
SET IDENTITY_INSERT [dbo].[product] OFF
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [fk_orderid] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([orderid])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [fk_orderid]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [fk_proid] FOREIGN KEY([productID])
REFERENCES [dbo].[product] ([id])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [fk_proid]
GO
ALTER TABLE [dbo].[product]  WITH CHECK ADD  CONSTRAINT [fk_cateid] FOREIGN KEY([cateID])
REFERENCES [dbo].[Category] ([cid])
GO
ALTER TABLE [dbo].[product] CHECK CONSTRAINT [fk_cateid]
GO
ALTER TABLE [dbo].[product]  WITH CHECK ADD  CONSTRAINT [fk_sellid] FOREIGN KEY([sell_ID])
REFERENCES [dbo].[Account] ([uID])
GO
ALTER TABLE [dbo].[product] CHECK CONSTRAINT [fk_sellid]
GO
USE [master]
GO
ALTER DATABASE [ProjectPRJ301] SET  READ_WRITE 
GO
