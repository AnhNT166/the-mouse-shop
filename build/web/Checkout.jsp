<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link href="css/checkout.css" rel="stylesheet">
<%@page import="Entity.Product, Entity.LineItem, Entity.Account" %>
<%@page import="Model.DAO" %>
<%@page import="java.util.Vector"%>
<%@page import="java.util.ArrayList, java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!------ Include the above in your HEAD tag ---------->

<div class="container wrapper">
    <div class="row cart-head">
        <div class="container">
            <div class="row">
                <p></p>
            </div>
            <div class="row">
                <div style="display: table; margin: auto;">
                    <span class="step step_complete"> <a href="Cart.jsp" class="check-bc">Cart</a> <span class="step_line step_complete"> </span> <span class="step_line backline"> </span> </span>
                    <span class="step step_complete"> <a href="#" class="check-bc">Checkout</a> <span class="step_line "> </span> <span class="step_line step_complete"> </span> </span>
                    <span class="step_thankyou check-bc step_complete">Thank you</span>
                </div>
            </div>
            <div class="row">
                <p></p>
            </div>
        </div>
    </div>

    <div class="row cart-body">
        <form class="form-horizontal" method="post" action="order">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-push-6 col-sm-push-6">
                REVIEW ORDER

                <div class="panel panel-info">
                    <div class="panel-heading">
                        Review Order <div class="pull-right"><small><a class="afix-1" href="Cart.jsp">Edit Cart</a></small></div>
                    </div>
                    <div class="panel-body">
                        <%
                List<LineItem> cart = (List<LineItem>) session.getAttribute("cart");
                double subtotal =0;
                if(cart!=null)
                for(LineItem lineItem: cart){
                    Product product = lineItem.getProduct();
                    int quantity = lineItem.getQuantity();
                    double itemTotal = product.getPrice() * quantity;
                    subtotal += itemTotal;
                        %>
                        <div class="form-group">
                            <div class="col-sm-3 col-xs-3">
                                <img class="img-responsive" src="<%=product.getImage()%>" />
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="col-xs-12"><%=product.getName()%></div>
                                <div class="col-xs-12"><small>Quantity:<span><%=quantity%></span></small></div>
                            </div>
                            <div class="col-sm-3 col-xs-3 text-right">
                                <h6><span>$</span><%=product.getPrice()%></h6>
                            </div>
                        </div>
                        <%}%>
                        <div class="form-group"><hr /></div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <strong>Subtotal</strong>
                                <div class="pull-right"><span>$</span><span><%=subtotal%></span></div>
                            </div>
                            <div class="col-xs-12">
                                <small>Shipping</small>
                                <div class="pull-right"><span>-</span></div>
                            </div>
                        </div>
                        <div class="form-group"><hr /></div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <strong>Order Total</strong>
                                <div class="pull-right"><span>$</span><span><%=subtotal%></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                REVIEW ORDER END
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-pull-6 col-sm-pull-6">
                SHIPPING METHOD
                <div class="panel panel-info">
                    <div class="panel-heading">Address</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <h4>Shipping Address</h4>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="span1"></div>
                            <div class="col-md-6 col-xs-12">
                                <strong>Name:</strong>
                                <input type="text" name="name" class="form-control" value="" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12"><strong>Address:</strong></div>
                            <div class="col-md-12">
                                <input type="text" name="address" class="form-control" value="" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12"><strong>Phone Number:</strong></div>
                            <div class="col-md-12"><input type="text" name="phone_number" class="form-control" value="" required=""/></div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button type="submit" class="btn btn-primary btn-submit-fix">Place Order</button>
                            </div>
                        </div>
                    </div>
                </div>
                SHIPPING METHOD END
            </div>

        </form>
    </div>
</div>
