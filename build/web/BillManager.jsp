<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>




<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Admin - Quản lý đơn hàng</title>
        <!-- Add your CSS styles or link to external stylesheets here -->
        <%@page import="Entity.Product, Entity.Category, Entity.Account, Entity.OrderDetail" %>
        <%@page import="java.util.Vector"%>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #ccc;
            padding: 10px;
            text-align: center;
        }
        th {
            background-color: #f2f2f2;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        a.button {
            display: inline-block;
            padding: 10px 20px;
            background-color: #337ab7;
            color: white;
            text-decoration: none;
            border-radius: 4px;
        }
        a.button:hover {
            background-color: #286090;
        }
    </style>
</head>
<body>
        <div class="container">
            <h1>Danh sách đơn hàng</h1>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Bill ID</th>
                        <th>ID Sản Phẩm</th>
                        <th>Giá Tiền</th>
                        <th>Số Lượng</th>
                        <th>Tổng giá sản phẩm</th>
                        <th>Chi Tiết Đơn hàng</th>
                    </tr>
                </thead>
                <tbody>
                    <%
          Vector<OrderDetail> vectorO=
                    (Vector<OrderDetail>)request.getAttribute("adminB");
                    %>
                    <%for(OrderDetail od:vectorO){%>
                    <tr>
                        <td><%= od.getOrderID() %></td>
                        <td><%= od.getProductID()%></td>
                        <td><%=od.getPrice()%></td>
                        <td><%=od.getQuantity()%></td>
                        <td><%=od.getTotalPrice()%>$</td>
                        <td>
                            <a href="admin?service=viewbill&vid=<%=od.getOrderID()%>">View</a>
                        </td>
                    </tr>
                    <%}%>
                </tbody>
            </table>
        </div>
    </body>
</html>

