<%@page import="Entity.Product, Entity.Category, Entity.Account" %>
<%@page import="java.util.Vector"%>
<%@ page import="jakarta.servlet.http.HttpSession" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Home</title>
        <link href="img/character-mouse-small.png" rel="icon">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="home">Mouse Shop</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-end" id="navbarsExampleDefault">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="home">Home</a>
                        </li>
                        <%
                        Account acc = (Account) session.getAttribute("acc");
                        if(acc!=null && acc.getIsAdmin() == 1){
                        %>
                        <li class="nav-item ">
                            <a class="nav-link" href="AdminManager.jsp">Admin Manager</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="#">Hello <%=acc.getUsername()%></a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="logout">Log Out</a>
                        </li>

                        <%
                            }else if (acc!=null && acc.getIsAdmin()==0 && acc.getIsSell()==1 ){
                        %>
                        <li class="nav-item ">
                            <a class="nav-link" href="productmanager">Product Manager</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="#">Hello <%=acc.getUsername()%></a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="logout">Log Out</a>
                        </li>
                        <%
                            } else if(acc!=null && acc.getIsAdmin()==0 && acc.getIsSell()==0){
                        %>
                        <li class="nav-item ">
                            <a class="nav-link" href="#">Hello <%=acc.getUsername()%></a>

                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="logout">Log Out</a>
                        </li>
                        <%} else{%>
                        <li class="nav-item">
                            <a class="nav-link" href="Login.jsp">Log in</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Register.jsp">Sign up</a>
                        </li>
                        <%}%>
                    </ul>
                    <form action="search" method="post" class="form-inline my-2 my-lg-0">
                        <div class="input-group input-group-sm">
                            <input name="txt" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Search">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary btn-number">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <a class="btn btn-success btn-sm ml-3" href="Cart.jsp">
                            <i class="fa fa-shopping-cart"></i> Cart
                            <!--                            <span class="badge badge-light">0</span>-->
                        </a>
                    </form>
                </div>
            </div>
        </nav>
        <section class="jumbotron text-center">
            <div class="container">
                <h1 class="jumbotron-heading">Mouse Shop</h1>
                <p class="lead text-muted mb-0">Shop bán chuột hàng Việt Nam chất lượng cao</p>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="#">Product</a></li>
                            <!--                            <li class="breadcrumb-item active" aria-current="page">Sub-category</li>-->
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">

                <div class="col-12 col-sm-3">
                    <%
                    Vector<Category> cCate=
                    (Vector<Category>)request.getAttribute("cCate");
                    %>
                    <div class="card bg-light mb-3">
                        <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-list"></i> Categories</div>
                        <%for(Category cat: cCate){%>
                        <ul class="list-group category_block">
                            <li class="list-group-item"><a href="category?cid=<%=cat.getCid()%>"><%=cat.getCname()%></a></li>
                        </ul>
                        <%}%>
                    </div>
                    <div class="card bg-light mb-3">
                        <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-list"></i> Sắp xếp theo giá</div>
                        <ul class="list-group category_block">
                            <li class="list-group-item"><a href="sortproduct?sort=asc">Giá tăng dần</a></li>
                            <li class="list-group-item"><a href="sortproduct?sort=desc">Giá giảm dần</a></li>
                        </ul>
                    </div>
                </div>



                <%
           Vector<Product> cVector=
                 (Vector<Product>)request.getAttribute("cVector");
                %>
                <div class="col-sm-9">
                    <div class="row">
                        <%for(Product pro:cVector){%> 
                       
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="card">
                                <img class="card-img-top" src="<%=pro.getImage()%>" alt="Card image cap">
                                <div class="card-body">
                                    <h4 class="card-title"><a href="product?pid=<%=pro.getId()%>" title="View Product"><%=pro.getName()%></a></h4>
                                    <p class="card-text"><%=pro.getTitle()%></p>
                                    <div class="row">
                                        <div class="col">
                                            <p class="btn btn-danger btn-block"><%=pro.getPrice()%>$</p>
                                        </div>
                                        <form action="add-to-cart?cid=<%=pro.getId()%>" method="post">
                                            <div>
                                                <button class="btn btn-success btn-block" type="submit">Add to cart</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <%}%>
                        
                    </div> 
                </div>
                <div class="col-12">
                    <nav aria-label="...">
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1">Previous</a>
                            </li>

                            <li class="page-item active">
                                <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
                            </li>


                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <jsp:include page="FooterCode.jsp"></jsp:include>


    </body>

</html>

