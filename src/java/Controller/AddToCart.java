/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Entity.LineItem;
import Entity.Product;
import Model.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author Tuấn Anh
 */
@WebServlet(name = "AddToCart", urlPatterns = {"/add-to-cart"})
public class AddToCart extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DAO dao = new DAO();
        HttpSession session = request.getSession();
        int id = Integer.parseInt(request.getParameter("cid"));
        List<LineItem> cart = null;
        // lấy thông tin chi tiết của sản phẩm dựa trên id đã lấy ở bước trước.
        Product result = dao.getProductDetail(id);
// Kiểm tra xem sản phẩm có tồn tại không.
        if (result != null) {
            //: Kiểm tra xem trong session đã có giỏ hàng chưa. 
            //  Nếu chưa có, tạo một danh sách mới để lưu trữ các mục trong giỏ hàng.
            if (session.getAttribute("cart") == null) {
                cart = new ArrayList<>();
            } else {
                //Nếu đã có giỏ hàng trong session, lấy danh sách giỏ hàng từ session.
                cart = (List<LineItem>) session.getAttribute("cart");
            }
            LineItem lineItem = new LineItem();
            lineItem.setProduct(result); //gan thong tin san pham vao LineItem
            lineItem.setQuantity(1); //Đặt số lượng của mục trong giỏ hàng là 1.
            cart.add(lineItem); //Thêm mục lineItem vào danh sách giỏ hàng.
            session.setAttribute("cart", cart);
        }
        response.sendRedirect("Cart.jsp");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
