/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Admin;

import Entity.Account;
import Entity.Category;
import Entity.OrderDetail;
import Entity.Orders;
import Entity.Product;
import Model.DAO;
import Model.DAOAccount;
import Model.DAOAdmin;
import Model.DAOOrder;
import Model.DAOProduct;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Vector;

/**
 *
 * @author Tuấn Anh
 */
@WebServlet(name = "AdminController", urlPatterns = {"/admin"})
public class AdminController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            DAO dao = new DAO();
            DAOProduct daop = new DAOProduct();
            DAOAccount daoa = new DAOAccount();
            DAOAdmin daoad = new DAOAdmin();
            DAOOrder daoor = new DAOOrder();
            String service = request.getParameter("service");
            if (service == null) {
                service = "product";
            }
            if (service.equals("customer")) {
                Vector<Account> vector = daoa.getAllCustomer();
                request.setAttribute("cusVector", vector);
                request.getRequestDispatcher("CustomerManager.jsp").forward(request, response);
            }
            if (service.equals("product")) {
                Vector<Product> vectorP = daop.getAllProduct();
                request.setAttribute("adminP", vectorP);
                request.getRequestDispatcher("AdminProduct.jsp").forward(request, response);
            }

            if (service.equals("del")) {
                String pid = request.getParameter("id");
                daoad.deleteCustomer(pid);
                response.sendRedirect("AdminManager.jsp");
            }
            if (service.equals("adelete")) {
                String pid = request.getParameter("id");
                daoad.deleteProduct(pid);
                response.sendRedirect("AdminManager.jsp");
            }
            if (service.equals("bill")) {
                Vector<OrderDetail> vectorO = daoor.getAllOrders();
                request.setAttribute("adminB", vectorO);
                request.getRequestDispatcher("BillManager.jsp").forward(request, response);
            }
            if (service.equals("viewbill")) {
                int billID = Integer.parseInt(request.getParameter("vid"));
                Vector<Orders> vectorOrder = daoor.viewOrderDetail(billID);
                request.setAttribute("vectorOrder", vectorOrder);
                request.getRequestDispatcher("ViewBillDetail.jsp").forward(request, response);
            }
            //day list product len web
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
