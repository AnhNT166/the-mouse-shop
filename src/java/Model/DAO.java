/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Entity.Account;

import Entity.Category;
import Entity.LineItem;
import Entity.OrderDetail;
import Entity.Product;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tuấn Anh
 */
public class DAO extends DBConnect {

    public Vector<Product> sortProductByPrice(String type) {
        Vector<Product> vector = new Vector<>();
        String sql = "select *from product order by price " + type;
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int id = rs.getInt(1);
                String pname = rs.getString(2);
                String image = rs.getString(3);
                double pprice = rs.getDouble(4);
                String ptitle = rs.getString(5);
                String pdes = rs.getString(6);
                Product pro = new Product(id, pname, image, pprice, ptitle, pdes);
                vector.add(pro);
            }
        } catch (Exception e) {
        }
        return vector;
    }
//    public Vector<OrderDetail> getOrderDetailbyID(int id) {
//        Vector<OrderDetail> vector = new Vector<OrderDetail>();
//        String sql = "select * from OrderDetail";
//        ResultSet rs = getData(sql);
//        try {
//            while (rs.next()) {
//                int oid = rs.getInt(1);
//                String name = rs.getString(2);
//                String address = rs.getString(3);
//                String phone = rs.getString(4);
//                String email = rs.getString(5);
//                double totalPrice = rs.getDouble(6);
//                Date orderDate = rs.getDate(7);
//                OrderDetail od = new OrderDetail(oid, name, address, phone, email, totalPrice, orderDate);
//                vector.add(od);
//            }
//        } catch (Exception e) {
//        }
//        return vector;
//    }

    public Vector<Account> getAllCustomer() {
        Vector<Account> vector = new Vector<Account>();
        String sql = "select *from Account where isSell = 0 and isAdmin = 0";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int pid = rs.getInt(1);
                String username = rs.getString(2);
                String password = rs.getString(3);
                int isSell = rs.getInt(4);

                int isAdmin = rs.getInt(5);
                Account acc = new Account(pid, username, password, isSell, isAdmin);
                vector.add(acc);
            }
        } catch (Exception e) {
        }
        return vector;
    }

    public Product getProductDetails(String pid) {
        Product pro = new Product();
        String sql = "select *from product where id = " + pid;
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int id = rs.getInt(1);
                String pname = rs.getString(2);
                String image = rs.getString(3);
                double pprice = rs.getDouble(4);
                String ptitle = rs.getString(5);
                String pdes = rs.getString(6);
                pro = new Product(id, pname, image, pprice, ptitle, pdes);
            }
        } catch (Exception e) {
        }
        return pro;

    }



    public Product getProductDetail(int pid) {
        Vector<Product> vector = new Vector<Product>();
        String sql = "select *from product where id = " + pid;
        ResultSet rs = getData(sql);
        Product pro = null;
        try {
            while (rs.next()) {
                int id = rs.getInt(1);
                String pname = rs.getString(2);
                String image = rs.getString(3);
                double pprice = rs.getDouble(4);
                String ptitle = rs.getString(5);
                String pdes = rs.getString(6);
                pro = new Product(id, pname, image, pprice, ptitle, pdes);
                vector.add(pro);
            }
        } catch (Exception e) {
        }
        return pro;

    }

//    public ItemsOrder getProductOnCart(String pid) {
//        Vector<ItemsOrder> vector = new Vector<ItemsOrder>();
//        String sql = "select *from product where id = " + pid;
//        ResultSet rs = getData(sql);
//        ItemsOrder it = null;
//        try {
//            while (rs.next()) {
//                int id = rs.getInt(1);
//                String pname = rs.getString(2);
//                String image = rs.getString(3);
//                double pprice = rs.getDouble(4);
//                int quan = rs.getInt(5);
//                it = new ItemsOrder(id, pname, image, pprice, quan);
//                vector.add(it);
//            }
//        } catch (Exception e) {
//        }
//        return it;
//
//    }
}
