/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Entity.Account;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tuấn Anh
 */
public class DAOAccount extends DBConnect{
     public Account login(String user, String pass) {
        String sql = "select *from account where [user] = '" + user + "'and pass = '" + pass + "'";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int uid = rs.getInt(1);
                String username = rs.getString(2);
                String password = rs.getString(3);
                int isSell = rs.getInt(4);
                int isAdmin = rs.getInt(5);
                Account acc = new Account(uid, username, password, isSell, isAdmin);
                return acc;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public Account checkAccIfExits(String user) {
        String sql = "select *from account where [user] = '" + user + "'";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int uid = rs.getInt(1);
                String username = rs.getString(2);
                String password = rs.getString(3);
                int isSell = rs.getInt(4);
                int isAdmin = rs.getInt(5);
                Account acc = new Account(uid, username, password, isSell, isAdmin);
                return acc;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void signUp(String user, String pass) throws SQLException {
        String sql = "insert into account values(?,?,0,0)";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, user);
            ps.setString(2, pass);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public Vector<Account> getAllCustomer() {
        Vector<Account> vector = new Vector<Account>();
        String sql = "select *from Account where isSell = 0 and isAdmin = 0";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int pid = rs.getInt(1);
                String username = rs.getString(2);
                String password = rs.getString(3);
                int isSell = rs.getInt(4);

                int isAdmin = rs.getInt(5);
                Account acc = new Account(pid, username, password, isSell, isAdmin);
                vector.add(acc);
            }
        } catch (Exception e) {
        }
        return vector;
    }
}
