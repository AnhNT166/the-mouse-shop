/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Entity.Category;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Tuấn Anh
 */
public class DAOCategory extends DBConnect {

    public Vector<Category> getAllCategory() {
        Vector<Category> vectorC = new Vector<Category>();
        String sql = "select *from Category";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int cid = rs.getInt(1);
                String cname = rs.getString(2);
                Category cate = new Category(cid, cname);
                vectorC.add(cate);
            }
        } catch (Exception e) {
        }
        return vectorC;
    }
}
