/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Entity.Account;
import Entity.OrderDetail;
import Entity.Orders;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tuấn Anh
 */
public class DAOOrder extends DBConnect {

    public Vector<Orders> viewOrderDetail(int billID) {
        Vector<Orders> vector = new Vector<>();
        String sql = "select *from orders where orderid = " + billID;
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int orid = rs.getInt(1);
                Date orderDate = rs.getDate(2);
                double price = rs.getDouble(3);
                String name = rs.getString(4);
                String address = rs.getString(5);
                String phone = rs.getString(6);
                Orders or = new Orders(orid, orderDate, price, name, address, phone);
                vector.add(or);
            }
        } catch (Exception e) {
        }
        return vector;
    }

    public Vector<OrderDetail> getAllOrders() {
        Vector<OrderDetail> vector = new Vector<>();
        String sql = "select *From OrderDetail";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int orid = rs.getInt(1);
                int proid = rs.getInt(2);
                double price = rs.getDouble(3);
                int quan = rs.getInt(4);
                double total = rs.getDouble(5);
                OrderDetail od = new OrderDetail(orid, proid, price, quan, total);
                vector.add(od);
            }
        } catch (Exception e) {
        }
        return vector;
    }

    public int addToOrder(Date orderDate, double total, String name, String address, String phone) {
        String sql = "INSERT INTO Orders([orderdate],[totalbill],[name],[address],[phone]) VALUES(?, ?, ?, ? ,?)";
        int orderId = 0;
        try {
            PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            java.sql.Date sqlOrderDate = new java.sql.Date(orderDate.getTime());
            ps.setDate(1, sqlOrderDate);
            ps.setDouble(2, total);
            ps.setString(3, name);
            ps.setString(4, address);
            ps.setString(5, phone);
            ps.executeUpdate();

            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                orderId = generatedKeys.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOOrder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return orderId;
    }

    public void addToOrderDetail(int orderId, int productId, double price, int quantity, double total) {
        String sql = "INSERT INTO OrderDetail([orderID],[productID],[price],[quantity],[totalbill]) VALUES(?, ?, ?, ?, ?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, orderId);
            ps.setInt(2, productId);
            ps.setDouble(3, price);
            ps.setInt(4, quantity);
            ps.setDouble(5, total);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOOrder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
