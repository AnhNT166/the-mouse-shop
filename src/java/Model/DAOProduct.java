/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Entity.Product;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tuấn Anh
 */
public class DAOProduct extends DBConnect {

    public Vector<Product> getProductBySellID(int sid) {
        Vector<Product> vector = new Vector<Product>();
        String sql = "select *from product where sell_ID = " + sid;
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int id = rs.getInt(1);
                String pname = rs.getString(2);
                String image = rs.getString(3);
                double pprice = rs.getDouble(4);
                String ptitle = rs.getString(5);
                String pdes = rs.getString(6);
                Product pro = new Product(id, pname, image, pprice, ptitle, pdes);
                vector.add(pro);
            }
        } catch (Exception e) {
        }
        return vector;

    }

    public Vector<Product> findByName(String name) {
        Vector<Product> vector = new Vector<Product>();
        String sql = "SELECT * FROM Product WHERE name LIKE '%" + name + "%'";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int pid = rs.getInt(1);
                String pname = rs.getString(2);
                String image = rs.getString(3);
                double pprice = rs.getDouble(4);
                String ptitle = rs.getString(5);
                String pdes = rs.getString(6);
                Product pro = new Product(pid, name, image, pprice, ptitle, pdes);
                vector.add(pro);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vector;
    }

    public Vector<Product> getProductByCategory(String cid) {
        Vector<Product> vector = new Vector<Product>();
        String sql = "select * from product where cateID = " + cid;
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int pid = rs.getInt(1);
                String pname = rs.getString(2);
                String image = rs.getString(3);
                double pprice = rs.getDouble(4);
                String ptitle = rs.getString(5);
                String pdes = rs.getString(6);
                Product pro = new Product(pid, pname, image, pprice, ptitle, pdes);
                vector.add(pro);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vector;
    }

    public void addProduct(String name, String image, String price, String title,
            String desscriptsion, String category, int sid) {
        String sql = "insert product(name,image,price,title,description,cateID,sell_ID) values(?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, image);
            ps.setString(3, price);
            ps.setString(4, title);
            ps.setString(5, desscriptsion);
            ps.setString(6, category);
            ps.setInt(7, sid);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public Vector<Product> sortProductByPrice(String type) {
        Vector<Product> vector = new Vector<Product>();
        String sql = "SELECT * FROM product ORDER BY price " + type;
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int pid = rs.getInt(1);
                String pname = rs.getString(2);
                String image = rs.getString(3);
                double pprice = rs.getDouble(4);
                String ptitle = rs.getString(5);
                String pdes = rs.getString(6);
                Product pro = new Product(pid, pname, image, pprice, ptitle, pdes);
                vector.add(pro);
            }
        } catch (Exception e) {
        }
        return vector;
    }
    
    public Vector<Product> getAllProduct() {
        Vector<Product> vector = new Vector<Product>();
        String sql = "select * from product";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int pid = rs.getInt(1);
                String pname = rs.getString(2);
                String image = rs.getString(3);
                double pprice = rs.getDouble(4);
                String ptitle = rs.getString(5);
                String pdes = rs.getString(6);
                Product pro = new Product(pid, pname, image, pprice, ptitle, pdes);
                vector.add(pro);
            }
        } catch (Exception e) {
        }
        return vector;
    }

    public void updateProduct(String name, String image, String price, String title,
            String desscriptsion, String category, String pid) {
        String sql = "update Product set [name] = ?, [image] = ?,[price] = ?,[title] = ?,[description]=?,[cateID]=? where id = ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, image);
            ps.setString(3, price);
            ps.setString(4, title);
            ps.setString(5, desscriptsion);
            ps.setString(6, category);
            ps.setString(7, pid);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
