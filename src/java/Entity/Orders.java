/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

import java.util.Date;

/**
 *
 * @author Tuấn Anh
 */
public class Orders {

    private int orderID;
    private Date orderDate;
    private double totalBill;
    private String name;
    private String address;
    private String phone;

    public Orders() {
    }

    public Orders(int orderID, Date orderDate, double totalBill, String name, String address, String phone) {
        this.orderID = orderID;
        this.orderDate = orderDate;
        this.totalBill = totalBill;
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public double getTotalBill() {
        return totalBill;
    }

    public void setTotalBill(double totalBill) {
        this.totalBill = totalBill;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Orders{" + "orderID=" + orderID + ", orderDate=" + orderDate + ", totalBill=" + totalBill + ", name=" + name + ", address=" + address + ", phone=" + phone + '}';
    }

}
