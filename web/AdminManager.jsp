<%-- 
    Document   : AdminManager
    Created on : Jul 14, 2023, 7:54:34 AM
    Author     : Tuấn Anh
--%>
<%@page import="Entity.Product, Entity.Category, Entity.Account" %>
<%@page import="java.util.Vector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <a class="nav-link" href="home">Home</a>
        <%
            Account acc = (Account) session.getAttribute("acc");
            if(acc!=null && acc.getIsAdmin() == 1){
        %>
            
            <a class="nav-link">Hello <%=acc.getUsername()%></a><br>
        <%}%>
        
        <a href="admin?service=customer">Customer Manager</a><br>
        <a href="admin?service=product">Product Manager</a><br>
        <a href="admin?service=bill">Bill Manager</a>

    </body>

</html>
