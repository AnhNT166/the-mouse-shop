<%-- 
    Document   : CustomerManager
    Created on : Jul 20, 2023, 4:35:37 PM
    Author     : Tuấn Anh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Entity.Product, Entity.Category, Entity.Account" %>
<%@page import="java.util.Vector"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
          Vector<Account> cusV=
                    (Vector<Account>)request.getAttribute("cusVector");
        %>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Delete Customer</th>
                </tr>
            </thead>
            <tbody>
                <%for(Account acc:cusV){%>
                <tr>
                    <td><%=acc.getuID()%></td>
                    <td><%=acc.getUsername()%></td>
                    <td><%=acc.getPassword()%></td>
                    <td>
                        <a href="admin?service=del&id=<%=acc.getuID()%>">Delete Customer</a>
                    </td>
                </tr>
                <%}%>
            </tbody>
        </table>
    </body>
</html>
