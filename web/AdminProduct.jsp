<%-- 
    Document   : AdminProduct
    Created on : Jul 20, 2023, 4:50:51 PM
    Author     : Tuấn Anh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="Entity.Product, Entity.Category, Entity.Account" %>
<%@page import="java.util.Vector"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
                    Vector<Product> listP=
                    (Vector<Product>)request.getAttribute("adminP");
        %>
        <div class="container-xl">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        
                        <div class="row">
                            <a href="home">Home</a>
                            <div class="col-sm-6">
                                <h2>Manage <b>Product</b></h2>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for(Product pro:listP){%>
                            <tr>
                                <td><%=pro.getId()%></td>
                                <td><%=pro.getName()%></td>
                                <td><%=pro.getPrice()%>$</td>
                                <td>
                                    <a href="admin?service=adelete&id=<%=pro.getId()%>">Delete</a>
                                </td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                    <div class="clearfix">
                        <div class="hint-text">Showing <b>1</b> out of <b>All</b> entries</div>
                        <ul class="pagination">
                            <li class="page-item disabled"><a href="#">Previous</a></li>
                            <li class="page-item active"><a href="#" class="page-link">1</a></li>
                        </ul>
                    </div>
                </div>
            </div>        
        </div>
    </body>
</html>
