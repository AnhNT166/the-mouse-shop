<%@page import="Entity.Product, Entity.LineItem, Entity.Account" %>
<%@page import="Model.DAO" %>
<%@page import="java.util.Vector"%>
<%@page import="java.util.ArrayList, java.util.List"%>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Total</th>
<!--                        <th>
                            <form action="DeleteAllCart" method="post">
                                <button type="submit" class="btn btn-danger">Remove All</button>
                            </form>
                        </th>-->
                    </tr>
                </thead>
                <%
                List<LineItem> cart = (List<LineItem>) session.getAttribute("cart");
                double subtotal =0;
                if(cart!=null)
                for(LineItem lineItem: cart){
                    Product product = lineItem.getProduct();
                    int quantity = lineItem.getQuantity();
                    double itemTotal = product.getPrice() * quantity;
                    subtotal += itemTotal;  
                %>
                <tbody>
                    <tr>
                        <td class="col-sm-8 col-md-6">
                            <div class="media">
                                <a class="thumbnail pull-left" href="#"> <img class="media-object" src="<%=product.getImage()%>" style="width: 72px; height: 72px;"> </a>
                                <div class="media-body">
                                    <h4 class="media-heading"><a href="#"><%=product.getName()%></a></h4>
                                </div>
                            </div>
                        </td>
                        </td>
                        <td class="col-sm-1 col-md-1" style="text-align: center">
                            <form action="UpdateCartQuantity" method="post">
                                <input type="hidden" name="productId" value="<%= product.getId() %>">
                                <input type="number" name="quantity" class="form-control quantityInput" value="<%= quantity %>" min="1">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                        </td>

                        <td class="col-sm-1 col-md-1 text-center">
                            <strong><%=product.getPrice()%></strong>
                        </td>
                        <td class="col-sm-1 col-md-1 text-center itemTotal">
                            <strong><%=itemTotal%></strong>
                        </td>


                        <td class="col-sm-1 col-md-1">
                            <form action="deleteprocart" method="post">
                                <input type="hidden" name="productId" value="<%=product.getId()%>">
                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span> Remove
                                </button>
                            </form>
                    </tr>

                    <%}%>


                <td>   </td>
                <td>   </td>
                <td>   </td>
                <td><h3>Total</h3></td>
                <td class="text-right"><h3><strong id="subtotal"><%=subtotal%></strong></h3></td>
                </tr>
                <tr>
                    <td>   </td>
                    <td>   </td>
                    <td>   </td>
                    <td><h5>Estimated shipping</h5></td>
                    <td class="text-right"><h5><strong>$0.00</strong></h5></td>
                </tr>
                <tr>
                    <td>   </td>
                    <td>   </td>
                    <td>   </td>
                    <td>
                        <a href="home">
                            <button type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
                            </button>
                        </a>
                    </td>

                    <td>
                        <form action="checkout" method="post">
                            <input type="hidden" name="cart" value="<%=cart%>">
                            <button type="submit" class="btn btn-success">
                                Checkout <span class="glyphicon glyphicon-play"></span>
                            </button>
                        </form>

                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>
