<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ page import="jakarta.servlet.http.HttpSession" %>

<%@page import="Entity.Product, Entity.LineItem, Entity.Account" %>
<%@page import="Model.DAO" %>
<%@page import="java.util.Vector"%>
<%@page import="java.util.ArrayList, java.util.List"%>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Thank You</title>
        <!-- Add your CSS styles or link to external stylesheets here -->
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h1>Thank You for Your Order!</h1>
                    <p>Your order has been successfully placed. Below is the summary of your order:</p>
                    <h3>Order Details</h3>
                    <p><strong>Name:</strong> <%= request.getParameter("name") %></p>
                    <p><strong>Address:</strong> <%= request.getParameter("address") %></p>
                    <p><strong>Phone Number:</strong> <%= request.getParameter("phone_number") %></p>

                    <h3>Order Items</h3>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                            List<LineItem> cart = (List<LineItem>) session.getAttribute("cart");
                            double subtotal = 0;
                            if (cart != null) {
                                for (LineItem lineItem : cart) {
                                    Product product = lineItem.getProduct();
                                    int quantity = lineItem.getQuantity();
                                    double itemTotal = product.getPrice() * quantity;
                                    subtotal += itemTotal;
                            %>
                            <tr>
                                <td><%= product.getName()%></td>
                                <td><%= quantity %></td>
                                <td>$<%= product.getPrice() %></td>
                                <td>$<%= itemTotal %></td>
                            </tr>
                            <% }} %>
                            <tr>
                                <td colspan="3" class="text-right"><strong>Subtotal</strong></td>
                                <td>$<%=subtotal%></td>
                            </tr>
                        </tbody>
                    </table>

                    <p><strong>Order Total: $<%= subtotal %></strong></p>
                    <p>Thank you for shopping with us!</p>
                    <a href="home">

                    </a>
                    <form action="home" method="post">
                        <button type="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
