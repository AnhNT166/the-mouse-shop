<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Admin - Quản lý đơn hàng</title>
        <%@page import="Entity.Product, Entity.Category, Entity.Account, Entity.Orders" %>
        <%@page import="java.util.Vector"%>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #ccc;
            padding: 10px;
            text-align: center;
        }
        th {
            background-color: #f2f2f2;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        a.button {
            display: inline-block;
            padding: 10px 20px;
            background-color: #337ab7;
            color: white;
            text-decoration: none;
            border-radius: 4px;
        }
        a.button:hover {
            background-color: #286090;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Chi Tiết Đơn Hàng</h1>
        <table>
            <thead>
                <tr>
                    <th>Bill ID</th>
                    <th>Ngày Đặt Hàng</th>
                    <th>Tổng Số Tiền Đơn Hàng</th>
                    <th>Tên Khách Hàng</th>
                    <th>Địa Chỉ Khách Hàng</th>
                    <th>Số ĐT Khách Hàng</th>
                </tr>
            </thead>
            <tbody>
                <% Vector<Orders> vectorO = (Vector<Orders>) request.getAttribute("vectorOrder"); %>
                <% for (Orders od : vectorO) { %>
                    <tr>
                        <td><%= od.getOrderID() %></td>
                        <td><%= od.getOrderDate() %></td>
                        <td><%= od.getTotalBill() %></td>
                        <td><%= od.getName() %></td>
                        <td><%= od.getAddress() %></td>
                        <td><%= od.getPhone() %></td>
                    </tr>
                <% } %>
            </tbody>
        </table>
        <div>
            <a href="home" class="button">Home</a>
        </div>
    </div>
</body>
</html>






